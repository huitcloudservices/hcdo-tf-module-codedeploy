resource "aws_codedeploy_app" "codedeploy-app" {
  name = "${lower(var.product)}-${var.environment}-${var.app_scope}"
}

resource "aws_iam_role_policy" "codedeploy-policy" {
  name   = "${lower(var.product)}-${var.environment}-${var.app_scope}-codedeploy-readonly-iam-policy"
  role   = "${aws_iam_role.autodeploy-role.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:CompleteLifecycleAction",
        "autoscaling:DeleteLifecycleHook",
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeLifecycleHooks",
        "autoscaling:PutLifecycleHook",
        "autoscaling:RecordLifecycleActionHeartbeat",
        "ec2:DescribeInstances",
        "ec2:DescribeInstanceStatus",
        "tag:GetTags",
        "tag:GetResources"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "autodeploy-role" {
  name               = "${lower(var.product)}-${var.environment}-${var.app_scope}-autodeploy-iam-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "codedeploy.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_codedeploy_deployment_group" "codedeploy-dg" {
  app_name               = "${aws_codedeploy_app.codedeploy-app.name}"
  deployment_group_name  = "${lower(var.product)}-${var.environment}-${var.app_scope}-dg"
  service_role_arn       = "${aws_iam_role.autodeploy-role.arn}"
  deployment_config_name = "${var.deployment_config_name}"
  autoscaling_groups     = ["${var.autoscaling_groups}"]
}

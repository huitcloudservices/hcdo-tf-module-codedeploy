variable "product" {
  description = "Product name as listed in Slash"
}

variable "environment" {
  description = "Environment name"
}

variable "app_scope" {
  description = "Application scope. i.e. app, node, master, slave, etc..."
  default     = "app"
}

variable "deployment_config_name" {
  description = "The name of the group's deployment config"
  default     = "CodeDeployDefault.OneAtATime"
}

variable "autoscaling_groups" {
  description = "Autoscaling groups associated with the deployment group"
  type        = "list"
  default     = []
}

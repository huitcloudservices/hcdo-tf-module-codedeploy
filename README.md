# HUIT Cloud & DevOps CodeDeploy Terraform module

## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| product | Product name as listed in Slash | - | yes |
| environment | Environment name | - | yes |
| app_scope | Application scope. i.e. app, node, master, slave, etc... | `"app"` | no |
| deployment_config_name | The name of the group's deployment config | `"CodeDeployDefault.OneAtATime"` | no |
| autoscaling_groups | Autoscaling groups associated with the deployment group | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| codedeploy_dg_id | The deployment group's ID |
| codedeploy_app_name | The group's assigned application |
| codedeploy_dg_name | The group's name |
| codedeploy_service_role_arn | The group's service role ARN |
| codedeploy_autoscaling_groups | The autoscaling groups associated with the deployment group |
| codedeploy_deployment_config_name | The name of the group's deployment config |


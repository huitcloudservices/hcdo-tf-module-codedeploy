// The deployment group's ID
output "codedeploy_dg_id" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.id}"
}

// The group's assigned application
output "codedeploy_app_name" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.app_name}"
}

// The group's name
output "codedeploy_dg_name" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.deployment_group_name}"
}

// The group's service role ARN
output "codedeploy_service_role_arn" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.service_role_arn}"
}

// The autoscaling groups associated with the deployment group
output "codedeploy_autoscaling_groups" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.autoscaling_groups}"
}

// The name of the group's deployment config
output "codedeploy_deployment_config_name" {
  value = "${aws_codedeploy_deployment_group.codedeploy-dg.deployment_config_name}"
}
